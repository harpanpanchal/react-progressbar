# React Progressbar Application

Steps to install the packages / dependencies.

1. 'npm install' to install the packages / dependencies.
2. 'npm start' to run the project. FYI: http://localhost:9000/

# Project File structure

1. All the components are stored inside 'Components' folder.
2. StyledComponent has been used for styling individual component. Also, https://semantic-ui.com and Sass has been used. However, there is less usage of SASS as StyledComponent has been used widely these days.
3. React hooks and Axios has been used as well.
