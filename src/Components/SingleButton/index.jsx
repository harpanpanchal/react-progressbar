import React from 'react';
import StyledButton from './StyledButton';

const SingleButton = ({ val, onClickHandler }) => {
  return (
    <StyledButton className='ui orange button' onClick={onClickHandler}>
      {val}
    </StyledButton>
  );
};

export default SingleButton;
