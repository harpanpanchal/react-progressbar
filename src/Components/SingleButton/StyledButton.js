import styled from 'styled-components';

const StyledButton = styled.button`
  margin: 20px 10px 0px 0px !important;
`;

export default StyledButton;
