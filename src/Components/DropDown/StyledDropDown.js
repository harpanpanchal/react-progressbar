import styled from 'styled-components';

const StyledDropDown = styled.select`
  margin: 20px 10px 0px 0px !important;
`;

export default StyledDropDown;
