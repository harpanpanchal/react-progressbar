import React from 'react';

const DropDown = ({val}) => {
    return val.map((item, index) => <option key={index} value={index}>#progress{index + 1}</option>)
};

export default DropDown;