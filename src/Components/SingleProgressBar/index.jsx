import React from 'react';
import StyledProgressBar from './StyledSingleProgressBar';

const SingleProgressBar = ({ val, limit }) =>
  val.map((item, index) => (
    <StyledProgressBar
      highlightprogress={item > limit ? true : false}
      key={index}
      value={item}
      max={limit}
    ></StyledProgressBar>
  ));

export default SingleProgressBar;
