import styled from 'styled-components';

const StyledProgressBar = styled.progress`
  display: block;
  height: 40px;
  margin: 10px 0px;
  width: 100%;
  -webkit-appearance: none;

  &::-webkit-progress-bar {
    background: #999999;
  }

  &::-webkit-progress-value {
    background: ${(props) => (props.highlightprogress ? '#d01919' : '#2185d0')};
    transition: 0.5s ease-out;
    -webkit-transition: 0.5s ease-out;
  }
`;

export default StyledProgressBar;
