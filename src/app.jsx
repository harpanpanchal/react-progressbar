import React, { useState, useEffect } from 'react';
import axios from 'axios';
import SingleButton from './Components/SingleButton';
import DropDown from './Components/DropDown/DropDown';
import StyledDropDown from './Components/DropDown/StyledDropDown';
import SingleProgressBar from './Components/SingleProgressBar';

const App = () => {
  const [data, setData] = useState([]);
  const [bar, setBar] = useState(0);

  const onBtnClickHandler = (val) => {
    const updatedBarValue = data.bars[bar] + val;
    const setBarVal = () => {
      if (updatedBarValue < 0) {
        return 0;
      } else if (updatedBarValue > data.limit) {
        return updatedBarValue;
      } else {
        return updatedBarValue;
      }
    };

    setData({
      ...data,
      ...(data.bars[bar] = setBarVal()),
    });
  };

  const onBarChangeHandler = (val) => {
    setBar(val);
  };

  useEffect(() => {
    async function fetchData() {
      const response = await axios('http://pb-api.herokuapp.com/bars');
      setData(...data, response.data);
    }
    fetchData();
  }, []);

  return (
    <>
      <h1 align='center'>React Progressbar</h1>
      {data && data.bars && data.limit && (
        <>
          <div className='ui container'>
            <SingleProgressBar limit={data.limit} val={data.bars} />
          </div>

          <div className='ui container'>
            <div className='ui form'>
              <div className='field'>
                <StyledDropDown
                  className='ui search dropdown'
                  onChange={(e) => onBarChangeHandler(e.target.value)}
                >
                  <DropDown val={data.bars} />
                </StyledDropDown>
              </div>
            </div>
          </div>
          <div className='ui container'>
            <div className='spaced'>
              {data &&
                data.buttons &&
                data.buttons.map((btn, index) => (
                  <SingleButton
                    key={index}
                    val={btn}
                    onClickHandler={() => onBtnClickHandler(btn)}
                  />
                ))}
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default App;
